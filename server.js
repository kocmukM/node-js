const WEBSOCKET = require('ws');
const SERVER = WEBSOCKET.Server;

let ws = new SERVER({port:3001});
let messages = [];

//1 минута
const SPAM_INTERVAL_MILLIS = 1000 * 60;


//20 сообщений в минуту - максимум
const SPAM_MESSAGE_COUNT = 20;

//3 минуты
const BLOCK_TIME_MILLIS  = 1000 * 60 * 3;

console.log('server enabled');

ws.on("connection",function(socket){
    proccesseNewConnection(socket)
});

function proccesseNewConnection(socket) {
    console.log('client connected');
    let currentUserMessages = [];
    let blockedAt = null;
    messages.forEach(function(msg){
       sendMessage(socket, 200, "success", msg)
    });

    socket.on('message',function(data){
        //Проверяем, заблокирован ли пользователь
        let blocked = checkIfBlocked(blockedAt);
        if(typeof (blocked) === 'number') {
            sendMessage(socket, 403, "forbidden", "Ожидайте еще "+blocked+" секунд", blocked);
            return
        }
        if(checkIfSpammer(currentUserMessages)) {
            blockedAt = new Date();
            sendMessage(socket, 403, "forbidden", "Вы заблокированы за спам на " + BLOCK_TIME_MILLIS / 1000 / 60 + " минут", BLOCK_TIME_MILLIS)
        } else {
            proccesseNewMessage(data, currentUserMessages)
        }
    });
}

function checkIfBlocked(blockedAt) {
    if(blockedAt == null)
        return false;
    let currentTime =  new Date().getTime();
    let estimateTime = blockedAt.getTime() + BLOCK_TIME_MILLIS;
    if(estimateTime > currentTime) {
        return (estimateTime - currentTime);
    }
    blockedAt =  null;
    return false;
}

function checkIfSpammer(userMessages) {
    let currentTime = new Date().getTime();
    let lastMessageCount = 0;
    for(let i = userMessages.length -1; i>=0; i--) {
        if(currentTime - userMessages[i].writtenAt.getTime() > SPAM_INTERVAL_MILLIS) {
            return false;
        }
        lastMessageCount ++;
        if(lastMessageCount >= SPAM_MESSAGE_COUNT) {
            return true;
        }
    }
    return false;
}

function proccesseNewMessage(data, currentUserMessages) {
    let message = new Message(data);
    messages.push(message);
    currentUserMessages.push(message);
    console.log('message send: '+data);
    sendForAll(200, "success", message)
}


function sendForAll(responseCode, responseMessage, message) {
    ws.clients.forEach(function(clientSocket){
        sendMessage(clientSocket, responseCode, responseMessage, message, null)
    });
}

function sendMessage(socket, responseCode, responseMessage, message, data) {
    let time = "";
    if(message instanceof Message) {
        time = message.writtenAt.getHours() + " : " + message.writtenAt.getMinutes() + " : " + message.writtenAt.getSeconds()
        socket.send(buildResponse(responseCode, responseMessage, time + "  ->    " + message.message, data));
    } else {
        socket.send(buildResponse(responseCode, responseMessage, message, data))
    }

}


function buildResponse(responseCode, responseMessage, message, data) {
    var response = {
        "responseCode": responseCode,
        "responseMessage": responseMessage,
        "data": data,
        "message": message
    };
    return JSON.stringify(response)
}

class Message {
    constructor(message) {
        this.writtenAt = new Date();
        this.message = message;
    }
}
