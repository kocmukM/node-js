const socket = new WebSocket('ws://localhost:3001');

//отправка сообщения из формы
document.forms.chat.onsubmit = function (event) {
    event.preventDefault();
    let outMsg = this.message.value;
    socket.send(outMsg);
};

document.addEventListener('DOMContentLoaded', function(){
    let blockedAtMillis = localStorage.getItem('blockedAtMillis');
    if(blockedAtMillis != null) {
        let blockPeriod = localStorage.getItem('blockPeriod');
        let blockRemaining = (parseInt(blockedAtMillis) + parseInt(blockPeriod)) - new Date().getTime();
        if(blockRemaining > 0 ) {
            console.log(blockRemaining);
            console.log(new Date().getTime());
            document.getElementById('block').style.display = 'block';
            startTimer(blockRemaining / 1000)
        } else {
            clearStorage()
        }
    }
});

//обработчик входящих сообщений
socket.onmessage = function (event) {

    let inMsg = JSON.parse(event.data);
    if (inMsg['responseCode'] >= 200 && inMsg['responseCode'] <= 300) {
        showMessage(inMsg["message"]);
    } else {
        proccesseError(inMsg)
    }
};

function proccesseError(inMsg) {
    switch (inMsg['responseCode']) {
        case 403 :
            document.getElementById('block').style.display = 'block';
            startTimer(parseInt(inMsg['data'] / 1000));
            localStorage.setItem('blockedAtMillis', new Date().getTime());
            localStorage.setItem('blockPeriod', inMsg['data']);
            break;
    }
}

function clearStorage() {
    localStorage.setItem('blockedAtMillis', null);
    localStorage.setItem('blockPeriod', null);
}

function startTimer(duration) {
    var timer = duration, minutes, seconds;
    let interval =  setInterval(function () {


        minutes = parseInt(timer / 60, 10)
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        document.getElementById('timer_value').innerText = minutes + " минут " + seconds + " секунд";
        console.log("tick" + timer);
        if (--timer  < 0) {
            document.getElementById('block').style.display = 'none';
            clearInterval(interval);
            clearStorage()
        }
    }, 1000);
}

function showMessage(msg) {
    let msgElement = document.createElement('p');
    msgElement.appendChild(document.createTextNode(msg));
    document.getElementById('chat').appendChild(msgElement);

}